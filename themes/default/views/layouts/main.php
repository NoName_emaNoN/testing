<!DOCTYPE html>
<html lang="<?php echo Yii::app()->language; ?>">
<head>
	<meta charset="<?php echo Yii::app()->charset; ?>" />
	<meta name="keywords" content="<?php echo $this->keywords; ?>" />
	<meta name="description" content="<?php echo $this->description; ?>" />
	<link rel="shortcut icon" href="<?php echo Yii::app()->theme->baseUrl; ?>/web/images/favicon.ico" />
	<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/web/css/main.css" />
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<script type="text/javascript">
	var baseUrl = '<?php echo Yii::app()->baseUrl?>';
</script>

<body>

<div class='container'>

	<h1><?php echo Yii::app()->getModule('yupe')->siteName; ?></h1>

	<?php $this->widget('application.modules.contentblock.widgets.ContentBlockWidget', array('code' => 'welcome_text')); ?>

	<?php $this->widget('YFlashMessages'); ?>
	<!-- flashMessages -->
	<div class="row-fliud">
		<?php echo $content; ?>
	</div>
</div>
<!-- page -->
<?php $this->widget(
	"application.modules.contentblock.widgets.ContentBlockWidget",
	array("code" => "STAT", "silent" => true)
); ?>
</body>
</html>