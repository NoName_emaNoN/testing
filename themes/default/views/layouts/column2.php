<?php $this->beginContent('//layouts/main'); ?>

<hr />
<div class="row">
	<div class="span9">
		<h2><?php echo $this->pageTitle; ?></h2>

		<?php $this->widget(
			'bootstrap.widgets.TbBreadcrumbs',
			array(
				'links' => $this->breadcrumbs,
			)
		); ?>

		<!-- content start-->
		<div class="content">
			<?php echo $content; ?>
		</div>
		<!-- content end-->
	</div>

	<!-- sidebar start -->
	<div class="span3 sidebar">
	</div>
	<!-- sidebar end -->

</div>
<?php $this->endContent(); ?>
