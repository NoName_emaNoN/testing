<?php

/* @var $this QuestionController */

$this->pageTitle = 'Начать тестирование';

$this->breadcrumbs = array('Начать тестирование');

?>

<div class="form well">

	<?php echo CHtml::beginForm(); ?>

	<div class="row-fluid">
		<?php echo CHtml::label('Сколько вопросов Вы хотели бы проработать сегодня?', 'count'); ?>
		<?php echo CHtml::textField('count', 10); ?>
	</div>

	<div class="row-fluid submit">
		<?php echo CHtml::submitButton('Начать тестирование', array('class' => 'btn btn-primary')); ?>
	</div>

	<?php echo CHtml::endForm(); ?>
</div>

<div class="row-fluid">
	<?php $this->widget('application.modules.contentblock.widgets.ContentBlockWidget', array('code' => 'start_text')); ?>
</div>
