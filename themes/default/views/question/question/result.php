<?php

/* @var $this QuestionController */
/* @var $session UserSession */

$this->pageTitle = 'Результаты тестирования';

?>

<p><b>Проработано вопросов: </b><?= $session->count ?></p>
<p><b>Правильных ответов: </b><?= round($session->getRightAnswerCount() / (int)$session->count * 100) ?>%</p>

<?php if ($session->getRightAnswerCount() != $session->count): ?>
	<p><b>Вопросы, в которых Вы допустили ошибку:</b></p>

	<ol>
		<?php foreach ($session->answers as $answer): ?>
			<?php if ($answer->answer !== $answer->question->answer): ?>
				<li>
					<p><?= $answer->question->text ?></p>

					<p><b>Варианты ответов:</b></p>

					<ul class="unstyled row-fluid">
						<?php foreach ($answer->question->getAnswers() as $value): ?>
							<li><?php echo $value; ?></li>
						<?php endforeach; ?>
					</ul>

					<p><b>Вы ответили:</b> <?= $answer->getAnswerName() ?></p>

					<p><b>Правильный ответ:</b> <?= $answer->question->getAnswerName() ?></p>

					<p><b>Объяснение: </b><?= $answer->question->explanation ?></p>
				</li>
			<?php endif; ?>
		<?php endforeach; ?>
	</ol>
<?php endif; ?>

<div class="row-fluid">
	<?php
	$this->widget(
		'bootstrap.widgets.TbButton',
		array(
			'buttonType' => 'link',
			'type'       => 'primary',
			'label'      => Yii::t('QuestionModule.question', 'Начать заново'),
			'url'        => Yii::app()->createUrl('/question/question/start'),
		)
	); ?>
</div>