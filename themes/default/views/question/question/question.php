<?php

/* @var $this QuestionController */
/* @var $question Question */
/* @var $session UserSession */
/* @var $answer UserSessionQuestion */

/* @var $form TbActiveForm */

$this->pageTitle = 'Вопрос №' . (count($session->answers) + 1) . ' из ' . $session->count;
?>

<?php $this->widget('application.modules.yupe.widgets.YFlashMessages'); ?>

<div class="row-fluid">
	<?php echo $question->text; ?>

	<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
			'id'           => 'question-form',
			'action'       => array('/question/question/check'),
			'type'         => 'vertical',
			'inlineErrors' => true,
			'htmlOptions'  => array(
				'class' => 'well',
			)
		)
	); ?>

	<?php echo $form->errorSummary($answer); ?>

	<div class='row-fluid control-group <?php echo $answer->hasErrors('answer') ? 'error' : ''; ?>'>
		<?php echo $form->radioButtonList($answer, 'answer', $question->getAnswers(), array('required' => true)); ?>
	</div>

	<?php echo $form->hiddenField($answer, 'question_id'); ?>

	<div class="row-fluid control-group">
		<?php
		$this->widget(
			'bootstrap.widgets.TbButton',
			array(
				'buttonType' => 'submit',
				'type'       => 'primary',
				'label'      => Yii::t('QuestionModule.question', 'Проверить'),
			)
		); ?>
	</div>

	<?php $this->endWidget(); ?>

	<p><b>ID:</b> <?php echo $question->id; ?></p>

	<?php
	$this->widget(
		'bootstrap.widgets.TbButton',
		array(
			'buttonType' => 'link',
			'type'       => 'warning',
			'url'        => array('/question/question/reset'),
			'label'      => Yii::t('QuestionModule.question', 'Начать заново'),
		)
	); ?>
	<?php
	$this->widget(
		'bootstrap.widgets.TbButton',
		array(
			'buttonType' => 'link',
			'type'       => 'danger',
			'url'        => array('/user/account/logout'),
			'label'      => Yii::t('QuestionModule.question', 'Выйти'),
		)
	); ?>

</div>