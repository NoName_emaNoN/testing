<?php

/* @var $this QuestionController */
/* @var $question Question */
/* @var $session UserSession */
/* @var $answer UserSessionQuestion */

/* @var $form TbActiveForm */

$this->pageTitle = 'Вопрос №' . ($session->answerCount) . ' из ' . $session->count;
?>

<?php $this->widget('application.modules.yupe.widgets.YFlashMessages'); ?>

<div class="row-fluid">
	<p><b>Правильный ответ: </b><?php echo $question->getAnswerName(); ?></p>

	<p>Объяснение: <?php echo $question->explanation; ?></p>

	<p><b>Вопрос:</b></p>
	<?php echo $question->text; ?>

	<p><b>Варианты ответов:</b></p>
	<ul class="unstyled">
		<?php foreach ($question->getAnswers() as $key => $value): ?>
			<?php if ($answer->answer == $key): ?>
				<li><b><?php echo $value; ?></b></li>
			<?php else: ?>
				<li><?php echo $value; ?></li>
			<?php endif; ?>
		<?php endforeach; ?>
	</ul>

	<div class="row-fluid control-group">
		<?php
		$this->widget(
			'bootstrap.widgets.TbButton',
			array(
				'buttonType' => 'link',
				'type'       => 'primary',
				'label'      => ($session->answerCount == $session->count) ? Yii::t('QuestionModule.question', 'Закончить тестирование') : Yii::t('QuestionModule.question', 'Следующий вопрос'),
				'url'        => Yii::app()->createUrl('/question/question/question'),
			)
		); ?>
	</div>

	<p><b>ID:</b> <?php echo $question->id; ?></p>

	<p><b>Область знаний: </b><?php echo $question->getThemeName(); ?></p>

	<?php
	$this->widget(
		'bootstrap.widgets.TbButton',
		array(
			'buttonType' => 'link',
			'type'       => 'warning',
			'url'        => array('/question/question/reset'),
			'label'      => Yii::t('QuestionModule.question', 'Начать заново'),
		)
	); ?>
	<?php
	$this->widget(
		'bootstrap.widgets.TbButton',
		array(
			'buttonType' => 'link',
			'type'       => 'danger',
			'url'        => array('/user/account/logout'),
			'label'      => Yii::t('QuestionModule.question', 'Выйти'),
		)
	); ?>

</div>