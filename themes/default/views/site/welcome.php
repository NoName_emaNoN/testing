<div class="row-fluid">
	<?php $this->widget('application.modules.contentblock.widgets.ContentBlockWidget', array('code' => 'guest_text')); ?>
</div>

<div class="row-fluid">
	<?php
	$this->widget(
		'bootstrap.widgets.TbButton',
		array(
			'buttonType' => 'link',
			'type'       => 'primary',
			'label'      => Yii::t('QuestionModule.question', 'Войти'),
			'url'        => Yii::app()->createUrl('/user/account/login'),
		)
	); ?>
</div>
