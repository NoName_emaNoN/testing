<?php

class QuestionthemeModule extends YWebModule
{
	public function getDependencies()
	{
		return array();
	}

	public function getVersion()
	{
		return Yii::t('QuestionthemeModule.questionTheme', '0.1');
	}

	public function getIsInstallDefault()
	{
		return false;
	}

	public function getCategory()
	{
		return Yii::t('QuestionthemeModule.questionTheme', 'Вопросы');
	}

	public function getName()
	{
		return Yii::t('QuestionthemeModule.questionTheme', 'Темы вопросов');
	}

	public function getDescription()
	{
		return Yii::t('QuestionthemeModule.questionTheme', 'Модуль для создания и редактирования тем вопросов');
	}

	public function getAuthor()
	{
		return Yii::t('QuestionthemeModule.questionTheme', 'Mikhail Chemezov');
	}

	public function getAuthorEmail()
	{
		return Yii::t('QuestionthemeModule.questionTheme', 'michlenanosoft@gmail.com');
	}

	public function getUrl()
	{
		return Yii::t('QuestionthemeModule.questionTheme', 'http://zexed.net');
	}

	public function getIcon()
	{
		return "th-list";
	}

	public function init()
	{
		parent::init();

		$this->setImport(array(
			'application.modules.questiontheme.models.*',
			'application.modules.questiontheme.components.*',
			'application.modules.questiontheme.components.widgets.*',
		));

		// Если у модуля не задан редактор - спросим у ядра
		if (!$this->editor)
			$this->editor = Yii::app()->getModule('yupe')->editor;
	}

	public function isMultiLang()
	{
		return false;
	}

	public function getNavigation()
	{
		return array(
			array('icon' => 'list-alt', 'label' => Yii::t('QuestionthemeModule.questionTheme', 'Список тем'), 'url' => array('/questiontheme/default/index')),
			array('icon' => 'plus-sign', 'label' => Yii::t('QuestionthemeModule.questionTheme', 'Добавить тему'), 'url' => array('/questiontheme/default/create')),
		);
	}

	public function beforeControllerAction($controller, $action)
	{
		if (parent::beforeControllerAction($controller, $action))
		{
			// this method is called before any module controller action is performed
			// you may place customized code here
			return true;
		}
		else
			return false;
	}
}
