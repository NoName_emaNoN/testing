<?php
/**
 * Отображение для _form:
 *
 * @category YupeView
 * @package  YupeCMS
 * @author   Yupe Team <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://yupe.ru
 **/

/* @var $form TbActiveForm */
$form = $this->beginWidget(
	'bootstrap.widgets.TbActiveForm', array(
		'id'                     => 'question-theme-form',
		'enableAjaxValidation'   => false,
		'enableClientValidation' => true,
		'type'                   => 'vertical',
		'htmlOptions'            => array('class' => 'well'),
		'inlineErrors'           => true,
	)
);
?>

	<div class="alert alert-info">
		<?php echo Yii::t('questiontheme', 'Поля, отмеченные'); ?>
		<span class="required">*</span>
		<?php echo Yii::t('questiontheme', 'обязательны.'); ?>
	</div>

<?php echo $form->errorSummary($model); ?>

	<div class="row-fluid control-group <?php echo $model->hasErrors('name') ? 'error' : ''; ?>">
		<?php echo $form->textFieldRow($model, 'name', array('class' => 'span5 popover-help', 'size' => 60, 'maxlength' => 255, 'data-original-title' => $model->getAttributeLabel('name'), 'data-content' => $model->getAttributeDescription('name'))); ?>
	</div>
	<div class="row-fluid control-group <?php echo $model->hasErrors('enable') ? 'error' : ''; ?>">
		<?php echo $form->dropDownListRow($model, 'enable', $model->statusList, array('class' => 'span5')); ?>
	</div>

<?php
$this->widget(
	'bootstrap.widgets.TbButton', array(
		'buttonType' => 'submit',
		'type'       => 'primary',
		'label'      => Yii::t('questiontheme', 'Сохранить тему вопроса и закрыть'),
	)
); ?>
<?php
$this->widget(
	'bootstrap.widgets.TbButton', array(
		'buttonType'  => 'submit',
		'htmlOptions' => array('name' => 'submit-type', 'value' => 'index'),
		'label'       => Yii::t('questiontheme', 'Сохранить тему вопроса и продолжить'),
	)
); ?>

<?php $this->endWidget(); ?>