<?php
/**
 * Отображение для view:
 *
 * @category YupeView
 * @package  YupeCMS
 * @author   Yupe Team <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://yupe.ru
 **/
$this->breadcrumbs = array(
	Yii::app()->getModule('questiontheme')->getCategory() => array(),
	Yii::t('questiontheme', 'Темы вопросов')              => array('/questiontheme/default/index'),
	$model->name,
);

$this->pageTitle = Yii::t('questiontheme', 'Темы вопросов - просмотр');

$this->menu = array(
	array('icon' => 'list-alt', 'label' => Yii::t('questiontheme', 'Управление темами вопросов'), 'url' => array('/questiontheme/default/index')),
	array('icon' => 'plus-sign', 'label' => Yii::t('questiontheme', 'Добавить тему вопроса'), 'url' => array('/questiontheme/default/create')),
	array('label' => Yii::t('questiontheme', 'Тема вопроса') . ' «' . mb_substr($model->id, 0, 32) . '»'),
	array('icon' => 'pencil', 'label' => Yii::t('questiontheme', 'Редактирование темы вопроса'), 'url' => array(
		'/questiontheme/default/update',
		'id' => $model->id
	)),
	array('icon' => 'eye-open', 'label' => Yii::t('questiontheme', 'Просмотреть тему вопроса'), 'url' => array(
		'/questiontheme/default/view',
		'id' => $model->id
	)),
	array('icon' => 'trash', 'label' => Yii::t('questiontheme', 'Удалить тему вопроса'), 'url' => '#', 'linkOptions' => array(
		'submit'  => array('/questiontheme/default/delete', 'id' => $model->id),
		'confirm' => Yii::t('questiontheme', 'Вы уверены, что хотите удалить тему вопроса?'),
	)),
);
?>
<div class="page-header">
	<h1>
		<?php echo Yii::t('questiontheme', 'Просмотр') . ' ' . Yii::t('questiontheme', 'темы вопроса'); ?><br />
		<small>&laquo;<?php echo $model->name; ?>&raquo;</small>
	</h1>
</div>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'       => $model,
	'attributes' => array(
		'id',
		'name',
		array(
			'name'  => 'enable',
			'value' => $model->getStatus(),
		),
	),
)); ?>
