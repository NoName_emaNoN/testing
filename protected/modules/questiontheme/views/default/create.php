<?php
/**
 * Отображение для create:
 *
 *   @category YupeView
 *   @package  YupeCMS
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
    $this->breadcrumbs = array(
        Yii::app()->getModule('questiontheme')->getCategory() => array(),
        Yii::t('questiontheme', 'Темы вопросов') => array('/questiontheme/default/index'),
        Yii::t('questiontheme', 'Добавление'),
    );

    $this->pageTitle = Yii::t('questiontheme', 'Темы вопросов - добавление');

    $this->menu = array(
        array('icon' => 'list-alt', 'label' => Yii::t('questiontheme', 'Управление темами вопросов'), 'url' => array('/questiontheme/default/index')),
        array('icon' => 'plus-sign', 'label' => Yii::t('questiontheme', 'Добавить тему вопроса'), 'url' => array('/questiontheme/default/create')),
    );
?>
<div class="page-header">
    <h1>
        <?php echo Yii::t('questiontheme', 'Темы вопросов'); ?>
        <small><?php echo Yii::t('questiontheme', 'добавление'); ?></small>
    </h1>
</div>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>