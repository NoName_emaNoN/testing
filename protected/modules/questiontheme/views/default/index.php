<?php
/**
 * Отображение для index:
 *
 * @category YupeView
 * @package  YupeCMS
 * @author   Yupe Team <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://yupe.ru
 **/
$this->breadcrumbs = array(
	Yii::app()->getModule('questiontheme')->getCategory() => array(),
	Yii::t('questiontheme', 'Темы вопросов')              => array('/questiontheme/default/index'),
	Yii::t('questiontheme', 'Управление'),
);

$this->pageTitle = Yii::t('questiontheme', 'Темы вопросов - управление');

$this->menu = array(
	array('icon' => 'list-alt', 'label' => Yii::t('questiontheme', 'Управление темами вопросов'), 'url' => array('/questiontheme/default/index')),
	array('icon' => 'plus-sign', 'label' => Yii::t('questiontheme', 'Добавить тему вопроса'), 'url' => array('/questiontheme/default/create')),
);
?>
<div class="page-header">
	<h1>
		<?php echo Yii::t('questiontheme', 'Темы вопросов'); ?>
		<small><?php echo Yii::t('questiontheme', 'управление'); ?></small>
	</h1>
</div>

<p> <?php echo Yii::t('questiontheme', 'В данном разделе представлены средства управления темами вопросов'); ?>
</p>

<?php
$this->widget('application.modules.yupe.components.YCustomGridView', array(
	'id'           => 'question-theme-grid',
	'type'         => 'condensed',
	'dataProvider' => $model->search(),
	'filter'       => $model,
	'columns'      => array(
		array(
			'name'        => 'id',
			'type'        => 'raw',
			'value'       => 'CHtml::link($data->id, array("/questiontheme/default/update", "id" => $data->id))',
			'htmlOptions' => array('width' => '50px'),
		),
		array(
			'name'  => 'name',
			'type'  => 'raw',
			'value' => 'CHtml::link($data->name, array("/questiontheme/default/view", "id" => $data->id))',
		),
		array(
			'name'   => 'enable',
			'type'   => 'raw',
			'value'  => '$this->grid->returnBootstrapStatusHtml($data, "enable", "Status")',
			'filter' => $model->getStatusList()
		),
		array(
			'class'       => 'bootstrap.widgets.TbButtonColumn',
			'htmlOptions' => array('width' => '70px'),
		),
	),
)); ?>
