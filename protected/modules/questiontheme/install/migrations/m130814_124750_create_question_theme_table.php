<?php

class m130814_124750_create_question_theme_table extends CDbMigration
{
	public function up()
	{
		$this->createTable('{{question_theme}}', array(
			'id'     => 'pk',
			'name'   => 'string',
			'enable' => 'boolean',
		));
	}

	public function down()
	{
		$this->dropTable('{{question_theme}}');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}