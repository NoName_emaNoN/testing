<?php
return array(
	'module'    => array(
		'class' => 'application.modules.questiontheme.QuestionthemeModule',
	),
	'import'    => array(
		'application.modules.questiontheme.models.*',
	),
	'component' => array(),
	'rules'     => array(),
);