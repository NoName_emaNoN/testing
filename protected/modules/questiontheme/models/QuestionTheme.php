<?php

/**
 * This is the model class for table "{{question_theme}}".
 *
 * The followings are the available columns in table '{{question_theme}}':
 * @property integer $id
 * @property string $name
 * @property integer $enable
 *
 * @method QuestionTheme enabled() return QuestionTheme model with "enabled" criteria
 */
class QuestionTheme extends YModel
{
	const STATUS_ENABLED = 1;
	const STATUS_DISABLED = 0;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{question_theme}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('enable', 'numerical', 'integerOnly' => true),
			array('name', 'length', 'max' => 255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, enable', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array();
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id'     => 'ID',
			'name'   => 'Название',
			'enable' => 'Доступность',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('name', $this->name, true);
		$criteria->compare('enable', $this->enable);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return QuestionTheme the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function getStatusList()
	{
		return array(
			self::STATUS_DISABLED => 'Недоступно',
			self::STATUS_ENABLED  => 'Доступно',
		);
	}

	public function getStatus()
	{
		$data = $this->statusList;

		return isset($data[$this->enable]) ? $data[$this->enable] : Yii::t('CategoryModule.category', '*неизвестно*');
	}

	public function scopes()
	{
		return array(
			'enabled' => array(
				'condition' => 'enable = :status',
				'params'    => array(':status' => self::STATUS_ENABLED),
			),
		);
	}
}
