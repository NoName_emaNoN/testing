<?php
/**
 * Отображение для _form:
 *
 *   @category YupeView
 *   @package  YupeCMS
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
$form = $this->beginWidget(
    'bootstrap.widgets.TbActiveForm', array(
        'id'                     => 'user-session-form',
        'enableAjaxValidation'   => false,
        'enableClientValidation' => true,
        'type'                   => 'vertical',
        'htmlOptions'            => array('class' => 'well'),
        'inlineErrors'           => true,
    )
);
?>

    <div class="alert alert-info">
        <?php echo Yii::t('question', 'Поля, отмеченные'); ?>
        <span class="required">*</span>
        <?php echo Yii::t('question', 'обязательны.'); ?>
    </div>

    <?php echo $form->errorSummary($model); ?>

    <div class="row-fluid control-group <?php echo $model->hasErrors('user_id') ? 'error' : ''; ?>">
        <?php echo $form->textFieldRow($model, 'user_id', array('class' => 'span3 popover-help', 'size' => 60, 'maxlength' => 60, 'data-original-title' => $model->getAttributeLabel('user_id'), 'data-content' => $model->getAttributeDescription('user_id'))); ?>
    </div>
    <div class="row-fluid control-group <?php echo $model->hasErrors('count') ? 'error' : ''; ?>">
        <?php echo $form->textFieldRow($model, 'count', array('class' => 'span3 popover-help', 'size' => 60, 'maxlength' => 60, 'data-original-title' => $model->getAttributeLabel('count'), 'data-content' => $model->getAttributeDescription('count'))); ?>
    </div>
    <div class="row-fluid control-group <?php echo $model->hasErrors('closed') ? 'error' : ''; ?>">
        <?php echo $form->textFieldRow($model, 'closed', array('class' => 'span3 popover-help', 'size' => 60, 'maxlength' => 60, 'data-original-title' => $model->getAttributeLabel('closed'), 'data-content' => $model->getAttributeDescription('closed'))); ?>
    </div>

    <?php
    $this->widget(
        'bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'type'       => 'primary',
            'label'      => Yii::t('question', 'Сохранить результат теста и закрыть'),
        )
    ); ?>
    <?php
    $this->widget(
        'bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'htmlOptions'=> array('name' => 'submit-type', 'value' => 'index'),
            'label'      => Yii::t('question', 'Сохранить результат теста и продолжить'),
        )
    ); ?>

<?php $this->endWidget(); ?>