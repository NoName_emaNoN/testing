<?php
/**
 * Отображение для update:
 *
 *   @category YupeView
 *   @package  YupeCMS
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
    $this->breadcrumbs = array(
        Yii::app()->getModule('question')->getCategory() => array(),
        Yii::t('question', 'Результаты тестов') => array('/userSession/index'),
        $model->id => array('/userSession/view', 'id' => $model->id),
        Yii::t('question', 'Редактирование'),
    );

    $this->pageTitle = Yii::t('question', 'Результаты тестов - редактирование');

    $this->menu = array(
        array('icon' => 'list-alt', 'label' => Yii::t('question', 'Управление результатами тестов'), 'url' => array('/userSession/index')),
        array('icon' => 'plus-sign', 'label' => Yii::t('question', 'Добавить результат теста'), 'url' => array('/userSession/create')),
        array('label' => Yii::t('question', 'Результат теста') . ' «' . mb_substr($model->id, 0, 32) . '»'),
        array('icon' => 'pencil', 'label' => Yii::t('question', 'Редактирование результата теста'), 'url' => array(
            '/userSession/update',
            'id' => $model->id
        )),
        array('icon' => 'eye-open', 'label' => Yii::t('question', 'Просмотреть результат теста'), 'url' => array(
            '/userSession/view',
            'id' => $model->id
        )),
        array('icon' => 'trash', 'label' => Yii::t('question', 'Удалить результат теста'), 'url' => '#', 'linkOptions' => array(
            'submit' => array('/userSession/delete', 'id' => $model->id),
            'confirm' => Yii::t('question', 'Вы уверены, что хотите удалить результат теста?'),
        )),
    );
?>
<div class="page-header">
    <h1>
        <?php echo Yii::t('question', 'Редактирование') . ' ' . Yii::t('question', 'результата теста'); ?><br />
        <small>&laquo;<?php echo $model->id; ?>&raquo;</small>
    </h1>
</div>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>