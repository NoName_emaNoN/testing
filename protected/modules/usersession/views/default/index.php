<?php
/**
 * Отображение для index:
 *
 * @category YupeView
 * @package  YupeCMS
 * @author   Yupe Team <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://yupe.ru
 **/
$this->breadcrumbs = array(
    Yii::app()->getModule('question')->getCategory() => array(),
    Yii::t('question', 'Результаты тестов')          => array('/usersession/default/index'),
    Yii::t('question', 'Управление'),
);

$this->pageTitle = Yii::t('question', 'Результаты тестов - управление');

$this->menu = array(
    array('icon' => 'list-alt', 'label' => Yii::t('question', 'Управление результатами тестов'), 'url' => array('/usersession/default/index')),
);
?>
<div class="page-header">
    <h1>
        <?php echo Yii::t('question', 'Результаты тестов'); ?>
        <small><?php echo Yii::t('question', 'управление'); ?></small>
    </h1>
</div>

<p> <?php echo Yii::t('question', 'В данном разделе представлены средства управления результатами тестов'); ?>
</p>

<?php
$this->widget(
    'application.modules.yupe.components.YCustomGridView',
    array(
        'id'           => 'user-session-grid',
        'type'         => 'condensed',
        'dataProvider' => $model->search(),
        'filter'       => $model,
        'columns'      => array(
            array(
                'name'        => 'id',
                'type'        => 'raw',
                'value'       => 'CHtml::link($data->id, array("/usersession/default/view", "id" => $data->id))',
                'htmlOptions' => array('width' => '50px'),
            ),
            array(
                'name'  => 'user_id',
                'type'  => 'raw',
                'value' => '$data->user->fullName',
            ),
            'count',
            array(
                'name'   => 'closed',
                'type'   => 'raw',
                'value'  => '$this->grid->returnBootstrapStatusHtml($data, "closed", "Closed")',
                'filter' => $model->getClosedList()
            ),
            array(
                'name'  => 'close_date',
                'value' => 'Yii::app()->dateFormatter->formatDateTime($data->close_date, "medium", "short")',
            ),
            array(
                'class'       => 'bootstrap.widgets.TbButtonColumn',
                'template'    => '{view}{delete}',
                'htmlOptions' => array('width' => '70px'),
            ),
        ),
    )
); ?>
