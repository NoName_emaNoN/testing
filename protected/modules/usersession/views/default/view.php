<?php
/**
 * Отображение для view:
 *
 * @category YupeView
 * @package  YupeCMS
 * @author   Yupe Team <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://yupe.ru
 **/

/* @var $model UserSession */
$this->breadcrumbs = array(
    Yii::app()->getModule('question')->getCategory() => array(),
    Yii::t('question', 'Результаты тестов')          => array('/usersession/default/index'),
    $model->id,
);

$this->pageTitle = Yii::t('question', 'Результаты тестов - просмотр');

$this->menu = array(
    array('icon' => 'list-alt', 'label' => Yii::t('question', 'Управление результатами тестов'), 'url' => array('/usersession/default/index')),
    array(
        'icon'  => 'eye-open',
        'label' => Yii::t('question', 'Просмотреть результат теста'),
        'url'   => array(
            '/usersession/default/view',
            'id' => $model->id
        )
    ),
//        array('icon' => 'trash', 'label' => Yii::t('question', 'Удалить результат теста'), 'url' => '#', 'linkOptions' => array(
//            'submit' => array('/usersession/default/delete', 'id' => $model->id),
//            'confirm' => Yii::t('question', 'Вы уверены, что хотите удалить результат теста?'),
//        )),
);
?>
<div class="page-header">
    <h1>
        <?php echo Yii::t('question', 'Просмотр') . ' ' . Yii::t('question', 'результата теста'); ?><br/>
        <small>&laquo;<?php echo $model->id; ?>&raquo;</small>
    </h1>
</div>

<?php $closed_list = $model->getClosedList(); ?>
<?php $this->widget(
    'bootstrap.widgets.TbDetailView',
    array(
        'data'       => $model,
        'attributes' => array(
            'id',
            array(
                'name'  => 'user_id',
                'value' => $model->user->getFullName(),
            ),
            'count',
            array(
                'name'  => 'closed',
                'value' => $closed_list[$model->closed],
            ),
            array(
                'label' => 'Правильных ответов',
                'value' => round($model->getRightAnswerCount() / $model->count * 100) . '%',
            ),
            array(
                'label' => 'Прорешано вопросов',
                'value' => round($model->answerCount / $model->count * 100) . '%',
            ),
        ),
    )
); ?>

<p><b>Вопросы, в которых допустили ошибку:</b></p>

<ol class="">
    <?php foreach ($model->answers as $answer): ?>
        <?php if ($answer->answer !== $answer->question->answer): ?>
            <li>
                <p><?= $answer->question->text ?></p>

                <p><b>Варианты ответов:</b></p>

                <ul class="unstyled row-fluid">
                    <?php foreach ($answer->question->getAnswers() as $value): ?>
                        <li><?php echo $value; ?></li>
                    <?php endforeach; ?>
                </ul>

                <p><b>Ответ:</b> <?= $answer->getAnswerName() ?></p>

                <p><b>Правильный ответ:</b> <?= $answer->question->getAnswerName() ?></p>

                <p><b>Объяснение: </b><?= $answer->question->explanation ?></p>

                <p><b>Область знаний: </b><?= isset($answer->question->theme) ? $answer->question->theme->name : '' ?></p>
            </li>
        <?php endif; ?>
    <?php endforeach; ?>
</ol>

