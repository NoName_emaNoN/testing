<?php
return array(
	'module'    => array(
		'class' => 'application.modules.usersession.UsersessionModule',
	),
	'import'    => array(
		'application.modules.question.models.*',
	),
	'component' => array(),
	'rules'     => array(),
);