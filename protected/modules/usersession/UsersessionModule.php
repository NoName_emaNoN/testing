<?php

class UsersessionModule extends YWebModule
{
	public function getDependencies()
	{
		return array(
			'questiontheme',
			'question',
		);
	}

	public function getVersion()
	{
		return Yii::t('QuestionModule.question', '0.1');
	}

	public function getIsInstallDefault()
	{
		return false;
	}

	public function getCategory()
	{
		return Yii::t('QuestionModule.question', 'Вопросы');
	}

	public function getName()
	{
		return Yii::t('QuestionModule.question', 'Результаты тестирования');
	}

	public function getDescription()
	{
		return Yii::t('QuestionModule.question', 'Модуль для просмотра результатов тестов');
	}

	public function getAuthor()
	{
		return Yii::t('QuestionModule.question', 'Mikhail Chemezov');
	}

	public function getAuthorEmail()
	{
		return Yii::t('QuestionModule.question', 'michlenanosoft@gmail.com');
	}

	public function getUrl()
	{
		return Yii::t('QuestionModule.question', 'http://zexed.net');
	}

	public function getIcon()
	{
		return "file";
	}

	public function init()
	{
		parent::init();

		$this->setImport(array(
			'application.modules.question.models.*',
			'application.modules.question.components.*',
			'application.modules.question.components.widgets.*',
		));

	}

	public function isMultiLang()
	{
		return false;
	}

	public function getNavigation()
	{
//		return array(
//			array('icon' => 'list-alt', 'label' => Yii::t('QuestionModule.question', 'Список вопросов'), 'url' => array('/question/default/index')),
//			array('icon' => 'plus-sign', 'label' => Yii::t('QuestionModule.question', 'Добавить вопрос'), 'url' => array('/question/default/create')),
//		);
	}

}
