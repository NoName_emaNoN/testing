<?php

class QuestionController extends YFrontController
{
    public $layout = '//layouts/column2';

    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    public function accessRules()
    {
        return array(
            array(
                'allow',
                'actions' => array('question', 'check', 'start', 'reset', 'result'),
                'users'   => array('@'),
            ),
            array(
                'deny',
                'users' => array('*'),
            )
        );
    }

    public function actionStart()
    {
        $session = UserSession::model()->findByAttributes(
            array(
                'user_id' => Yii::app()->user->id,
                'closed'  => UserSession::STATUS_OPENED,
            )
        );

        if ($session) {
            $this->redirect(array('/question/question/question'));
        }

        if (Yii::app()->request->isPostRequest && !empty($_POST['count']) && intval($_POST['count']) && (intval($_POST['count']) >= 1)) {
            $count = intval($_POST['count']);

            $session = new UserSession();
            $session->count = $count;
            $session->save();

            $this->redirect(array('/question/question/question'));
        }

        $this->render('start');
    }

    public function actionQuestion()
    {
        $session = $this->getSession();

        if ($session->answerCount == $session->count) {
            //end
            $session->closed = UserSession::STATUS_CLOSED;
            $session->close_date = new CDbExpression('NOW()');
            if ($session->save()) {
                $this->redirect(array('/question/question/result', 'session' => $session->id));
            } else {
                throw new CHttpException(500, 'Произошла ошибка при сохранении сессии');
            }
        }

        /* Let search a next question */

        $criteria = new CDbCriteria();

        if ($session->answerCount > 0) {
            $questions = array();

            foreach ($session->answers as $answer) {
                $questions[] = $answer->question_id;
            }

            $criteria->addNotInCondition('id', $questions);
        }

        $criteria->order = 'RAND()';

        /* @var $question Question */
        $question = Question::model()->find($criteria);

        if (!$question) {
            // Закончились вопросы :(
            $session->closed = UserSession::STATUS_CLOSED;
            $session->close_date = new CDbExpression('NOW()');
            if ($session->save()) {
                $this->redirect(array('/question/question/result', 'session' => $session->id));
            } else {
                throw new CHttpException(500, 'Произошла ошибка при сохранении сессии');
            }
        }

        $answer = new UserSessionQuestion();
        $answer->session_id = $session->id;
        $answer->question_id = $question->id;

        $this->render(
            'question',
            array(
                'question' => $question,
                'session'  => $session,
                'answer'   => $answer,
            )
        );
    }

    public function actionCheck()
    {
        $session = $this->getSession();

        $answer = new UserSessionQuestion();

        if (isset($_POST['UserSessionQuestion'])) {
            $answer->attributes = $_POST['UserSessionQuestion'];
            $answer->session_id = $session->id;

            if ($answer->save()) {
                $question = $answer->question;

                if ($question->answer === $answer->answer) {
                    Yii::app()->user->setFlash(
                        YFlashMessages::SUCCESS_MESSAGE,
                        Yii::t('QuestionModule.question', 'Правильно!')
                    );
                } else {
                    Yii::app()->user->setFlash(
                        YFlashMessages::ERROR_MESSAGE,
                        Yii::t('QuestionModule.question', 'Неверно!')
                    );
                }
            } else {
                die('shit happens');
            }

            $this->render(
                'check',
                array(
                    'answer'   => $answer,
                    'question' => $question,
                    'session'  => $session
                )
            );
        } else {
            $this->redirect(array('/question/question/question'));
        }

    }

    public function actionReset()
    {
        $session = $this->getSession();

        $session->closed = UserSession::STATUS_ABORTED;
        $session->close_date = new CDbExpression('NOW()');

        if ($session->save()) {
            $this->redirect(array('/question/question/start'));
        } else {
            throw new CHttpException(500, 'Произошла ошибка при сохранении сессии');
        }
    }

    public function actionResult($session)
    {
        $session = UserSession::model()->findByPk($session);

        if (!$session) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }

        $this->render('result', array('session' => $session));
    }

    /**
     * @return UserSession
     */
    public function getSession()
    {
        /* @var $session UserSession */
        $session = UserSession::model()->findByAttributes(
            array(
                'user_id' => Yii::app()->user->id,
                'closed'  => UserSession::STATUS_OPENED,
            )
        );

        if (!$session) {
            $this->redirect(array('/'));
        }

        return $session;
    }

}