<?php
/**
 * Отображение для index:
 *
 * @category YupeView
 * @package  YupeCMS
 * @author   Yupe Team <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://yupe.ru
 **/
$this->breadcrumbs = array(
	Yii::app()->getModule('question')->getCategory() => array(),
	Yii::t('question', 'Вопросы')                    => array('/question/default/index'),
	Yii::t('question', 'Управление'),
);

$this->pageTitle = Yii::t('question', 'Вопросы - управление');

$this->menu = array(
	array('icon' => 'list-alt', 'label' => Yii::t('question', 'Управление вопросами'), 'url' => array('/question/default/index')),
	array('icon' => 'plus-sign', 'label' => Yii::t('question', 'Добавить вопрос'), 'url' => array('/question/default/create')),
);
?>
<div class="page-header">
	<h1>
		<?php echo Yii::t('question', 'Вопросы'); ?>
		<small><?php echo Yii::t('question', 'управление'); ?></small>
	</h1>
</div>

<p> <?php echo Yii::t('question', 'В данном разделе представлены средства управления вопросами'); ?>
</p>

<?php
$this->widget('application.modules.yupe.components.YCustomGridView', array(
	'id'           => 'question-grid',
	'type'         => 'condensed',
	'dataProvider' => $model->search(),
	'filter'       => $model,
	'columns'      => array(
		array(
			'name'        => 'id',
			'type'        => 'raw',
			'value'       => 'CHtml::link($data->id, array("/question/default/update", "id" => $data->id))',
			'htmlOptions' => array('width' => '50px'),
		),
		array(
			'name'   => 'theme_id',
			'value'  => '$data->getThemeName()',
			'filter' => CHtml::listData($this->module->getThemeList(), 'id', 'name'),
			'htmlOptions' => array('width' => '170px'),
		),
		array(
			'name'  => 'text',
			'type'  => 'raw',
			'value' => '$data->text',
		),
		array(
			'class'       => 'bootstrap.widgets.TbButtonColumn',
			'htmlOptions' => array('width' => '70px'),
		),
	),
)); ?>
