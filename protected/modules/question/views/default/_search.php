<?php
/**
 * Отображение для _search:
 *
 *   @category YupeView
 *   @package  YupeCMS
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
$form = $this->beginWidget(
    'bootstrap.widgets.TbActiveForm', array(
        'action'      => Yii::app()->createUrl($this->route),
        'method'      => 'get',
        'type'        => 'vertical',
        'htmlOptions' => array('class' => 'well'),
    )
);
?>

    <fieldset class="inline">
        <div class="row-fluid control-group">
            <div class="span2">
                <?php echo $form->textFieldRow($model, 'id', array('class' => 'span3 popover-help', 'size' => 60, 'maxlength' => 60, 'data-original-title' => $model->getAttributeLabel('id'), 'data-content' => $model->getAttributeDescription('id'))); ?>
            </div>
            <div class="span2">
                <?php echo $form->textFieldRow($model, 'theme_id', array('class' => 'span3 popover-help', 'size' => 60, 'maxlength' => 60, 'data-original-title' => $model->getAttributeLabel('theme_id'), 'data-content' => $model->getAttributeDescription('theme_id'))); ?>
            </div>
            <div class="span2">
                <?php echo $form->textAreaRow($model, 'text', array('class' => 'span5 popover-help', 'rows' => 6, 'cols' => 50, 'data-original-title' => $model->getAttributeLabel('text'), 'data-content' => $model->getAttributeDescription('text'))); ?>
            </div>
            <div class="span2">
                <?php echo $form->textAreaRow($model, 'option1', array('class' => 'span5 popover-help', 'rows' => 6, 'cols' => 50, 'data-original-title' => $model->getAttributeLabel('option1'), 'data-content' => $model->getAttributeDescription('option1'))); ?>
            </div>
            <div class="span2">
                <?php echo $form->textAreaRow($model, 'option2', array('class' => 'span5 popover-help', 'rows' => 6, 'cols' => 50, 'data-original-title' => $model->getAttributeLabel('option2'), 'data-content' => $model->getAttributeDescription('option2'))); ?>
            </div>
            <div class="span2">
                <?php echo $form->textAreaRow($model, 'option3', array('class' => 'span5 popover-help', 'rows' => 6, 'cols' => 50, 'data-original-title' => $model->getAttributeLabel('option3'), 'data-content' => $model->getAttributeDescription('option3'))); ?>
            </div>
            <div class="span2">
                <?php echo $form->textAreaRow($model, 'option4', array('class' => 'span5 popover-help', 'rows' => 6, 'cols' => 50, 'data-original-title' => $model->getAttributeLabel('option4'), 'data-content' => $model->getAttributeDescription('option4'))); ?>
            </div>
            <div class="span2">
                <?php echo $form->textFieldRow($model, 'answer', array('class' => 'span3 popover-help', 'size' => 60, 'maxlength' => 60, 'data-original-title' => $model->getAttributeLabel('answer'), 'data-content' => $model->getAttributeDescription('answer'))); ?>
            </div>
            <div class="span2">
                <?php echo $form->textAreaRow($model, 'explanation', array('class' => 'span5 popover-help', 'rows' => 6, 'cols' => 50, 'data-original-title' => $model->getAttributeLabel('explanation'), 'data-content' => $model->getAttributeDescription('explanation'))); ?>
            </div>
        </div>
    </fieldset>

    <?php
    $this->widget(
        'bootstrap.widgets.TbButton', array(
            'type'        => 'primary',
            'encodeLabel' => false,
            'buttonType'  => 'submit',
            'label'       => '<i class="icon-search icon-white">&nbsp;</i> ' . Yii::t('question', 'Искать вопрос'),
        )
    ); ?>

<?php $this->endWidget(); ?>