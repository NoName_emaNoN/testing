<?php
/**
 * Отображение для create:
 *
 *   @category YupeView
 *   @package  YupeCMS
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
    $this->breadcrumbs = array(
        Yii::app()->getModule('question')->getCategory() => array(),
        Yii::t('question', 'Вопросы') => array('/question/default/index'),
        Yii::t('question', 'Добавление'),
    );

    $this->pageTitle = Yii::t('question', 'Вопросы - добавление');

    $this->menu = array(
        array('icon' => 'list-alt', 'label' => Yii::t('question', 'Управление вопросами'), 'url' => array('/question/default/index')),
        array('icon' => 'plus-sign', 'label' => Yii::t('question', 'Добавить вопрос'), 'url' => array('/question/default/create')),
    );
?>
<div class="page-header">
    <h1>
        <?php echo Yii::t('question', 'Вопросы'); ?>
        <small><?php echo Yii::t('question', 'добавление'); ?></small>
    </h1>
</div>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>