<?php
/**
 * Отображение для view:
 *
 * @category YupeView
 * @package  YupeCMS
 * @author   Yupe Team <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://yupe.ru
 **/

/* @var $model Question */
$this->breadcrumbs = array(
	Yii::app()->getModule('question')->getCategory() => array(),
	Yii::t('question', 'Вопросы')                    => array('/question/default/index'),
	$model->id,
);

$this->pageTitle = Yii::t('question', 'Вопросы - просмотр');

$this->menu = array(
	array('icon' => 'list-alt', 'label' => Yii::t('question', 'Управление вопросами'), 'url' => array('/question/default/index')),
	array('icon' => 'plus-sign', 'label' => Yii::t('question', 'Добавить вопрос'), 'url' => array('/question/default/create')),
	array('label' => Yii::t('question', 'Вопрос') . ' «' . mb_substr($model->id, 0, 32) . '»'),
	array('icon' => 'pencil', 'label' => Yii::t('question', 'Редактирование вопроса'), 'url' => array(
		'/question/default/update',
		'id' => $model->id
	)),
	array('icon' => 'eye-open', 'label' => Yii::t('question', 'Просмотреть вопрос'), 'url' => array(
		'/question/default/view',
		'id' => $model->id
	)),
	array('icon' => 'trash', 'label' => Yii::t('question', 'Удалить вопрос'), 'url' => '#', 'linkOptions' => array(
		'submit'  => array('/question/default/delete', 'id' => $model->id),
		'confirm' => Yii::t('question', 'Вы уверены, что хотите удалить вопрос?'),
	)),
);
?>
<div class="page-header">
	<h1>
		<?php echo Yii::t('question', 'Просмотр') . ' ' . Yii::t('question', 'вопроса'); ?><br />
		<small>&laquo;<?php echo $model->id; ?>&raquo;</small>
	</h1>
</div>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'       => $model,
	'attributes' => array(
		'id',
		array(
			'name'  => 'theme_id',
			'type'  => 'raw',
			'value' => CHtml::link($model->getThemeName(), array('/questiontheme/default/update', 'id' => $model->theme_id)),
		),
		array(
			'name' => 'text',
			'type' => 'raw',
		),
		'option1',
		'option2',
		'option3',
		'option4',
		array(
			'name'  => 'answer',
			'value' => $model->getAnswerName(),
		),
		'explanation',
	),
)); ?>
