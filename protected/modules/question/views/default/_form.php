<?php
/**
 * Отображение для _form:
 *
 * @category YupeView
 * @package  YupeCMS
 * @author   Yupe Team <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://yupe.ru
 **/
$form = $this->beginWidget(
	'bootstrap.widgets.TbActiveForm', array(
		'id'                     => 'question-form',
		'enableAjaxValidation'   => false,
		'enableClientValidation' => true,
		'type'                   => 'vertical',
		'htmlOptions'            => array('class' => 'well'),
		'inlineErrors'           => true,
	)
);
?>

	<div class="alert alert-info">
		<?php echo Yii::t('question', 'Поля, отмеченные'); ?>
		<span class="required">*</span>
		<?php echo Yii::t('question', 'обязательны.'); ?>
	</div>

<?php echo $form->errorSummary($model); ?>

	<div class="row-fluid control-group <?php echo $model->hasErrors('theme_id') ? 'error' : ''; ?>">
		<?php echo $form->dropDownListRow($model, 'theme_id', CHtml::listData($this->module->getThemeList(), 'id', 'name'), array('empty' => Yii::t('QuestionModule.question', '--выберите--'), 'class' => 'span5 popover-help', 'data-original-title' => $model->getAttributeLabel('theme_id'), 'data-content' => $model->getAttributeDescription('theme_id'))); ?>
	</div>
	<div class="row-fluid control-group <?php echo $model->hasErrors('text') ? 'error' : ''; ?>">
		<div class="popover-help" data-original-title='<?php echo $model->getAttributeLabel('text'); ?>' data-content='<?php echo $model->getAttributeDescription('text'); ?>'>
			<?php echo $form->labelEx($model, 'text'); ?>
			<?php
			$this->widget(
				$this->module->editor, array(
					'model'     => $model,
					'attribute' => 'text',
					'options'   => $this->module->editorOptions,
				)
			); ?>
		</div>
	</div>
	<div class="row-fluid control-group <?php echo $model->hasErrors('option1') ? 'error' : ''; ?>">
		<?php echo $form->textAreaRow($model, 'option1', array('class' => 'span5 popover-help', 'rows' => 6, 'cols' => 50, 'data-original-title' => $model->getAttributeLabel('option1'), 'data-content' => $model->getAttributeDescription('option1'))); ?>
	</div>
	<div class="row-fluid control-group <?php echo $model->hasErrors('option2') ? 'error' : ''; ?>">
		<?php echo $form->textAreaRow($model, 'option2', array('class' => 'span5 popover-help', 'rows' => 6, 'cols' => 50, 'data-original-title' => $model->getAttributeLabel('option2'), 'data-content' => $model->getAttributeDescription('option2'))); ?>
	</div>
	<div class="row-fluid control-group <?php echo $model->hasErrors('option3') ? 'error' : ''; ?>">
		<?php echo $form->textAreaRow($model, 'option3', array('class' => 'span5 popover-help', 'rows' => 6, 'cols' => 50, 'data-original-title' => $model->getAttributeLabel('option3'), 'data-content' => $model->getAttributeDescription('option3'))); ?>
	</div>
	<div class="row-fluid control-group <?php echo $model->hasErrors('option4') ? 'error' : ''; ?>">
		<?php echo $form->textAreaRow($model, 'option4', array('class' => 'span5 popover-help', 'rows' => 6, 'cols' => 50, 'data-original-title' => $model->getAttributeLabel('option4'), 'data-content' => $model->getAttributeDescription('option4'))); ?>
	</div>
	<div class="row-fluid control-group <?php echo $model->hasErrors('answer') ? 'error' : ''; ?>">
		<?php echo $form->dropDownListRow($model, 'answer', $model->answerList, array('class' => 'span3')); ?>
	</div>
	<div class="row-fluid control-group <?php echo $model->hasErrors('explanation') ? 'error' : ''; ?>">
		<?php echo $form->textAreaRow($model, 'explanation', array('class' => 'span5 popover-help', 'rows' => 6, 'cols' => 50, 'data-original-title' => $model->getAttributeLabel('explanation'), 'data-content' => $model->getAttributeDescription('explanation'))); ?>
	</div>

<?php
$this->widget(
	'bootstrap.widgets.TbButton', array(
		'buttonType' => 'submit',
		'type'       => 'primary',
		'label'      => Yii::t('question', 'Сохранить вопрос и закрыть'),
	)
); ?>
<?php
$this->widget(
	'bootstrap.widgets.TbButton', array(
		'buttonType'  => 'submit',
		'htmlOptions' => array('name' => 'submit-type', 'value' => 'index'),
		'label'       => Yii::t('question', 'Сохранить вопрос и продолжить'),
	)
); ?>

<?php $this->endWidget(); ?>