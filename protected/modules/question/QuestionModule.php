<?php

class QuestionModule extends YWebModule
{
	public function getDependencies()
	{
		return array(
			'questiontheme',
		);
	}

	public function getParamsLabels()
	{
		return array(
			'editor' => Yii::t('PageModule.page', 'Визуальный редактор'),
		);
	}

	public function getEditableParams()
	{
		return array(
			'editor' => Yii::app()->getModule('yupe')->editors,
		);
	}

	public function getVersion()
	{
		return Yii::t('QuestionModule.question', '0.1');
	}

	public function getIsInstallDefault()
	{
		return false;
	}

	public function getCategory()
	{
		return Yii::t('QuestionModule.question', 'Вопросы');
	}

	public function getName()
	{
		return Yii::t('QuestionModule.question', 'Вопросы');
	}

	public function getDescription()
	{
		return Yii::t('QuestionModule.question', 'Модуль для создания и редактирования вопросов');
	}

	public function getAuthor()
	{
		return Yii::t('QuestionModule.question', 'Mikhail Chemezov');
	}

	public function getAuthorEmail()
	{
		return Yii::t('QuestionModule.question', 'michlenanosoft@gmail.com');
	}

	public function getUrl()
	{
		return Yii::t('QuestionModule.question', 'http://zexed.net');
	}

	public function getIcon()
	{
		return "file";
	}

	public function init()
	{
		parent::init();

		$this->setImport(array(
			'application.modules.question.models.*',
			'application.modules.question.components.*',
			'application.modules.question.components.widgets.*',
		));

		// Если у модуля не задан редактор - спросим у ядра
		if (!$this->editor)
			$this->editor = Yii::app()->getModule('yupe')->editor;
	}

	public function isMultiLang()
	{
		return false;
	}

	public function getNavigation()
	{
		return array(
			array('icon' => 'list-alt', 'label' => Yii::t('QuestionModule.question', 'Список вопросов'), 'url' => array('/question/default/index')),
			array('icon' => 'plus-sign', 'label' => Yii::t('QuestionModule.question', 'Добавить вопрос'), 'url' => array('/question/default/create')),
		);
	}

	public function getThemeList()
	{
		$criteria = array('order' => 'id ASC');

		return QuestionTheme::model()->enabled()->findAll($criteria);
	}
}
