<?php

/**
 * This is the model class for table "{{question}}".
 *
 * The followings are the available columns in table '{{question}}':
 * @property integer $id
 * @property integer $theme_id
 * @property string $text
 * @property string $option1
 * @property string $option2
 * @property string $option3
 * @property string $option4
 * @property integer $answer
 * @property string $explanation
 *
 * @property QuestionTheme $theme
 */
class Question extends YModel
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{question}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('theme_id, text, option1, option2, option3, option4, answer', 'required'),
			array('theme_id, answer', 'numerical', 'integerOnly' => true),
			array('text, option1, option2, option3, option4, explanation', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, theme_id, text, option1, option2, option3, option4, answer, explanation', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'theme' => array(self::BELONGS_TO, 'QuestionTheme', 'theme_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id'          => 'ID',
			'theme_id'    => 'Тема вопроса',
			'text'        => 'Текст вопроса',
			'option1'     => 'Вариант A',
			'option2'     => 'Вариант B',
			'option3'     => 'Вариант C',
			'option4'     => 'Вариант D',
			'answer'      => 'Ответ',
			'explanation' => 'Объяснение',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('theme_id', $this->theme_id);
		$criteria->compare('text', $this->text, true);
		$criteria->compare('option1', $this->option1, true);
		$criteria->compare('option2', $this->option2, true);
		$criteria->compare('option3', $this->option3, true);
		$criteria->compare('option4', $this->option4, true);
		$criteria->compare('answer', $this->answer);
		$criteria->compare('explanation', $this->explanation, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Question the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function getAnswerList()
	{
		return array(
			1 => 'A',
			2 => 'B',
			3 => 'C',
			4 => 'D',
		);
	}

	public function getAnswers()
	{
		return array(
			1 => 'A. ' . $this->option1,
			2 => 'B. ' . $this->option2,
			3 => 'C. ' . $this->option3,
			4 => 'D. ' . $this->option4,
		);
	}

	public function getAnswerName()
	{
		$data = $this->getAnswerList();

		return isset($data[$this->answer]) ? $data[$this->answer] : Yii::t('QuestionModule.question', '*неизвестно*');
	}

	public function getThemeName()
	{
		return (isset($this->theme)) ? $this->theme->name : false;
	}
}
