<?php

/**
 * This is the model class for table "{{question_user_session}}".
 *
 * The followings are the available columns in table '{{question_user_session}}':
 * @property integer $id
 * @property integer $user_id
 * @property integer $count
 * @property integer $closed
 * @property string $close_date
 *
 * @property User $user
 * @property UserSessionQuestion[] $answers
 * @property integer $answerCount
 */
class UserSession extends YModel
{
    const STATUS_OPENED = 0;
    const STATUS_CLOSED = 1;
    const STATUS_ABORTED = 2;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{question_user_session}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('user_id, count, closed', 'numerical', 'integerOnly' => true),
            array('count', 'numerical', 'min' => 1),
            array('close_date', 'unsafe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, user_id, count, closed', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'user'        => array(self::BELONGS_TO, 'User', 'user_id'),
            'answers'     => array(self::HAS_MANY, 'UserSessionQuestion', 'session_id'),
            'answerCount' => array(self::STAT, 'UserSessionQuestion', 'session_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id'         => 'ID',
            'user_id'    => 'Пользователь',
            'count'      => 'Количество вопросов',
            'closed'     => 'Закрыта',
            'close_date' => 'Дата тестирования',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('user_id', $this->user_id);
        $criteria->compare('count', $this->count);
        $criteria->compare('closed', $this->closed);
        $criteria->compare('close_date', $this->close_date, true);

        return new CActiveDataProvider(
            $this, array(
                'criteria' => $criteria,
            )
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return UserSession the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    protected function beforeSave()
    {
        if (parent::beforeSave()) {
            if ($this->isNewRecord) {
                $this->closed = self::STATUS_OPENED;
                $this->user_id = Yii::app()->user->id;
            }

            return true;
        } else {
            return false;
        }
    }

    public function afterDelete()
    {
        foreach ($this->answers as $answer) {
            $answer->delete();
        }

        parent::afterDelete();
    }

    public function getClosedList()
    {
        return array(
            self::STATUS_OPENED  => 'Открыто',
            self::STATUS_CLOSED  => 'Закрыто',
            self::STATUS_ABORTED => 'Прервано',
        );
    }

    public function getRightAnswerCount()
    {
        $count = 0;

        foreach ($this->answers as $answer) {
            if ($answer->question->answer == $answer->answer) {
                $count++;
            }
        }

        return $count;
    }
}
