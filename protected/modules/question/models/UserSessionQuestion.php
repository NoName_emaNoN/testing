<?php

/**
 * This is the model class for table "{{question_user_session_question}}".
 *
 * The followings are the available columns in table '{{question_user_session_question}}':
 * @property integer $id
 * @property integer $session_id
 * @property integer $question_id
 * @property integer $answer
 *
 * @property UserSession $session
 * @property Question $question
 */
class UserSessionQuestion extends YModel
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{question_user_session_question}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('session_id, question_id, answer', 'numerical', 'integerOnly' => true),
			array('answer', 'in', 'range' => range(1, 4)),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, session_id, question_id, answer', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'session'  => array(self::BELONGS_TO, 'UserSession', 'session_id'),
			'question' => array(self::BELONGS_TO, 'Question', 'question_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id'          => 'ID',
			'session_id'  => 'Session',
			'question_id' => 'Question',
			'answer'      => 'Answer',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('session_id', $this->session_id);
		$criteria->compare('question_id', $this->question_id);
		$criteria->compare('answer', $this->answer);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return UserSessionQuestion the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function getAnswerName()
	{
		$list = $this->question->getAnswerList();
		return $list[$this->answer];
	}
}
