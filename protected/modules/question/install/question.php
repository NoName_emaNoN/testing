<?php
return array(
	'module'    => array(
		'class' => 'application.modules.question.QuestionModule',
	),
	'import'    => array(
		'application.modules.question.models.*',
	),
	'component' => array(),
	'rules'     => array(
		'/'                     => '/question/question/start',
		'/question'             => '/question/question/question',
		'/check'                => '/question/question/check',
		'/reset'                => '/question/question/reset',
		'/result/<session:\d+>' => '/question/question/result',
	),
);