<?php

class m141113_051547_add_close_date extends CDbMigration
{
    public function up()
    {
        $this->addColumn('{{question_user_session}}', 'close_date', 'DATETIME');
    }

    public function down()
    {
        $this->dropColumn('{{question_user_session}}', 'close_date');
    }

    /*
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}