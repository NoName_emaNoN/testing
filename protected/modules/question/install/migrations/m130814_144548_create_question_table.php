<?php

class m130814_144548_create_question_table extends CDbMigration
{
	public function up()
	{
		$this->createTable('{{question}}', array(
			'id'          => 'pk',
			'theme_id'    => 'integer',
			'text'        => 'text',
			'option1'     => 'text',
			'option2'     => 'text',
			'option3'     => 'text',
			'option4'     => 'text',
			'answer'      => 'int(1) NOT NULL',
			'explanation' => 'text',
		));

		$this->createIndex('theme_id', '{{question}}', 'theme_id');
	}

	public function down()
	{
		$this->dropTable('{{question}}');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}