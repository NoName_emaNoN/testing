<?php

class m130814_200823_create_user_session_table extends CDbMigration
{
	public function up()
	{
		$this->createTable('{{question_user_session}}', array(
			'id'      => 'pk',
			'user_id' => 'integer',
			'count'   => 'integer',
			'closed'  => 'boolean',
		));

		$this->createIndex('user_id', '{{question_user_session}}', 'user_id');
		$this->createIndex('closed', '{{question_user_session}}', 'closed');
	}

	public function down()
	{
		$this->dropTable('{{question_user_session}}');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}