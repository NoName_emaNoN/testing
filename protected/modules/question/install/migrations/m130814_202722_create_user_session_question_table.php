<?php

class m130814_202722_create_user_session_question_table extends CDbMigration
{
	public function up()
	{
		$this->createTable('{{question_user_session_question}}', array(
			'id'          => 'pk',
			'session_id'  => 'integer',
			'question_id' => 'integer',
			'answer'      => 'int(1)',
		));

		$this->createIndex('session_id', '{{question_user_session_question}}', 'session_id');
		$this->createIndex('question_id', '{{question_user_session_question}}', 'question_id');

		$this->createIndex('session_question', '{{question_user_session_question}}', 'session_id, question_id', true);
	}

	public function down()
	{
		$this->dropTable('{{question_user_session_question}}');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}